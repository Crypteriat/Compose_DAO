## Introduction Outline:

**0. Functional Programming and Composition**



**1. Sovereign Security & Web 3**

Internet users today face a number of cybersecurity issues. One major problem has been the centralization of user names and passwords. The current version of the internet, called Web2, is dominated by companies that provide services in exchange for your personal data. Mistrust and attacks on the internet services holding this data can make the user's information very vulnerable. From social media to important financial accounts, Compose DAO ensures that the user remains in control of their digital life. Zero knowledge like technologies such as Secure Remote Password (SRP) would improve internet security and leverage the Ethereum Name System (ENS) with agents acting on behalf of end-users. Through sovereign security, Compose DAO aims to provide everyday users a smooth transition from Web2 into Web3, giving back users ownership of their data.

**2. Supporting Web 3 via Fog Computing**


One of the main concepts of Web3 is decentralizing big tech companies and effectively taking back control the use of consumers data. The data these companies collect and resell to third-party advertisers are first gathered on our smart phones and then distributed to centralized data centers where they are stored. This not ony gives internet services control of our data but also puts our data at risk from attackers and mistrust. This is were fog computing comes into play. Fog computing is the idea of a distributed network composed of devices that can carry out computation, communication and storage. This architecture allows a middle layer between the collection of data from our phones and what is ultimately sent out to these data centers. Fog computing enables more control of users data by analyzing and determing the information before it is sent back. This process allows users to take over ownership of their private information and feel safe when utilizing the internet.

**3. Building a Congealed Network**
Congealed Network appears as new CVMs are created. These are uncensorable because they appear initially as web apps (WAs). They upload and maintain only when in use.


**4. The Compose Virtual Machine**

Our Compose Virtual Machine (CVM) plays an important role in the Compose DAO ecosystem because it ultimately acts as a foundation to bridge different blockchains together. The CVM utilizes modern development frameworks combined with our 1155+ smart contract family. CVM is broken up into three stages: ReactJs and Typescript on the fronted, DenoJs on the middle stack, and smart contract code on the backend. It is designed to accomplish and power multiple tasks which are to be run on our [iMynCoin](./Glossary.md) terminals and in return will support the growth of our congealed network. These various functions include capabilty to create [ephemeral](./Glossary.md) links and [PWAs](./Glossary.md) that will connect users to Web3 applications, for example [PairX](./Glossary.md).

**5. ERC/EIP 1155+: A new family of smart contracts**

To make Compose DAO possible, we are proposing and implementing a set of new smart contract standards for cryptocurrencies to adopt that will enable transactions to occur between different blockchains. At a high-level, these methods enable decentralized apps (DApps) to perform wrapping, composition, L1/L2 rollups and rolldowns, and then ultimately atomic swaps. Initially, we are targeting to bridge the Bitcoin and Ethereum ecosystems together. To achieve this bridge we are using the underlying Lightning Bitcoin (LBTC) infactrucutre, Point Time Locked Contracts (PTLC) and Discreet Log Contracts (DLC) along with the existing Ethereum Virtual Machine (EVM).

**6. PAIR: A yield generating non-fiat stablecoin based on the silver-gold ratio**

With the current state of the economy both in the United States and across the globe, it's clearly seen that the fiat system is not stable. Rising inflation coupled with numerous other problems has caused the value of the dollar to decrease and has left people wondering what new financial system will arise. We at Compose DAO are proposing a new financial instrument, one that is derived from two of the most popular and stable commidities, gold and silver. This is what we are calling Pair stablecoin. The Pair stablecoin is composed of a ratio between gold and silver that precisely keeps it at a consistent value which also leaves opportunity for arbitrage to occur. This in return enables silver and gold holders to tokenize their stake by providing assets which then mint Pair and essenitally allows them to generate yield on their holdings. Pair stablecoin provides people with a much less volatile and trustworthy financial measurement.

**7. PAIRx: Agent-based Dynamic Automated Market Making**
PAIRx, the oracle of exchanges between PAIR and its leveraged token C|D, or other tokens such as ETC, ETH, LTBC or BTC requires an automated maket maker. Automated Market Making, AMM refers to the "islands" of liquidity created at each atomic swap.


**8. Tokenomics & DAO Value Accrual**

Compose DAO has two primary tokens: CD token and Pair stablecoin, and these provide different utilities throughout the DAO ecosystem. The Pair stablecoin, as described above, provides a stable finacial instrument to measure values of various cryptocurrencies mainly in our PAIRx exchange as well as provide opportunitites to generate yield. The CD token represents Compose DAO's value and then more importantly gives token holders voting capabilities. Holders of CD token are allowed to propose changes for Compose DAO as well as vote on proposals to better the orginization's structure.

**9. Governance & Treasury**

For every DAO, governance protocols and their treasury play a key role in how the DAO operates and their efficiency to grow. Strong governance is defined by how coherent community members are able to propose and implement new changes to better the DAO. Compose DAO outlines this secure framework to allow continous improvements and ensures that our DAO is operating at the highest level. These same guidelines apply to our treasury management, Compose DAO members will collectively determine how and who are funds are supported. The treasury will fund and support child DAOs as well as ensure that contributors are paid for their work.
