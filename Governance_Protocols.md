# Composable DAO Governance Protocols

The intention of this section is to provide current and future community members insight to the governance protocols that drive Composable DAO. These protocols are aimed to be implemented in such a way to leverage the decision making governed by the token holders of Composable DAO. Governance protocols and voting regulation are executed via smart contracts to minimize risk and operate at an autonomous level. This enables members of the community to systematically come to a general consensus and continously improve the structure of Composable DAO. To participate in the voting process, one must hold CD token which allows holders to vote on improvement propoals. Examples of these proposals may inlcude the spending of treasury funds or changing the parameters of governance protocols.

NOTE: This document is a work in progress. Token amounts and timelines are subject to change. However, these will be finalized before completion of the DAO.

<br>

## Composable Improvement Proposals

Anyone holding CD token is able to vote for CD Improvement Proposals (CDIP). These proposals are to be executed after the completion of a thorough review process and when a majority vote is reached. Smart contracts will faciliate the voting process as well as proposal execution. Members (token holders) are able to bring forward proposals in our community discussions and when a popular conseus is made, an improvement proposal can be created. Individual proposers are restricted to one proposal at a time and cannot create another when one proposal is active or pending. 

### Deposit Period

After an improvement proposal is sumbitted, it enters a deposit period of two weeks. During this period CD is deposited as collateral to back the proposal. The total minimum deposit must reach 100 CD within two weeks from the time of proposal sumbission. This threshold is met when the sum of the initial deposit from the proposal handler as well as all other deposits from supporting members meet or exceed 100 CD. These parameters protects against spam and unnecessary proposals.

Deposits get refunded if all of the following conditions are met:

- The minimum deposit of 100 CD is reached within the two-week deposit period

- The total number of votes is greater than 40% of all staked CD

- The total number of `NoWithVeto` votes is less than 33.4% of the total vote.

- A vote returns a majority of `Yes` or `No` votes

Deposits are burned under any of the following conditions:

- The minimum deposit of 100 CD is not reached within the two-week deposit period

- The number of total votes after the two-week voting period is less than 40% of all staked CD

- The total number of `NoWithVeto` votes is less than 33.4% of the total vote

### Voting Process

If the minimum deposit threshold is met, the improvement proposal then enters a two-week voting period where token holders can vote for the proposal to be implemented. After the two-week voting period ends, the votes are tallied.

The voting options are:

- `Yes` - In favor, meaning they support the improvement proposal
- `No` - Not in favor, where they are against the proposal
- `NoWithVeto` - Veto, denoting they do not support the proposal and that the deposited funds should be burned as they deem the proposal spam

The proposal passes if the following requirements are met:

- At least 40% of all staked CD must vote.

- The total number of `NoWithVeto` votes is less than 33.4% of the total vote.

- The number of `Yes` votes reaches a 50% majority. 

If the above conditions are not met, the proposal is rejected. If the conditions are met, then the changes outlined in the proposal automatically get put into effect by the proposal handler.

<br>

## Protocol Evolution

One important feature of Composable DAO is enabling token holders to set the course for upcoming protocols. These governance protocols are constantly being evaluated and refined to reflect the current state of the economy and to be in the best interest of the community. In return, this allows the DAO's strucutre to evolve over time and define new standards for future governance protocols.

<br>

## Payout Structure 

Within the CD community, bounties will be rewarded upon completion of specific tasks. The bounty prizes are determined based on the difficulty of the task measured using the Fibonacci Agile Estimation. The bounty will be payed out after an approving merge request is merged.

<br>

## Community Discussions 

Community members are able to discuss improvements and propose protocols in both our Slack and Discord channels.