# Pair Exchange

### Overview

Currently decentralized exanchanges ([DEX](./Glossary.md)) provide enormorus value to the [DeFi](./Glossary.md) ecosystem because it allows crypto traders to facilitate peer-to-peer transactions without an intermediary or overhead. This is achieved by using smart contracts. Liquidity providers supply the pools by depositing the equivalent value of each asset for various pairs and in return, generate yield for providing liquidity that then other crypto traders can employ. However, fundamentally these DEXs turn out to be inefficient and in return create problems like [slippage](./Glossary.md) because the liquidity pools are isolated. Pair Exchange or [PairX](./Glossary.md) solves this problem by providing a more efficient DEX, one that values every cryptocurrency with a common base stablecoin called [Pair](./Glossary.md). This approach establishes a large network-like exchange where the liquidity for various cryptocurrencies becomes one large pool and greatly reduces the amount of slippage that can occur. In short, this design disaggreates the liquidity pools and produces a significantly more systematic decentralized exchange. To achieve this new level, PairX is powered by the underyling core components that ulmitaley make up Composeable DAO.


### PairX Oracle 

Because the PairX exchange is built on top of our low-latency internet infrasctructure, it can provide the most up-to-date and reliable price feed. As these swaps are executed and the market value adjusts, anyone using the PairX oracle service is guranteed the fastest liquidity data and historical prices. This oracle service unlocks a wide range of utilities and is yet another feature within Composable DAO.