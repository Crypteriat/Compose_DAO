This is work in progress...
# Project Page

## Introduction

Silver and Gold have been prized for centuries as a storehouse of wealth. These metals are used for more than jewelry and utensils. Today, they are in cell phones, computers, solar panels and other electronics due to their natural conducive properties. We once lived in a world where money was understood to be bimetallic ETPs. The Coinage Act of 1792 first defined the dollar relative to silver.  That remained until the 1900s, when the gold standard arose. However, on August 15, 1971 President Nixon abandoned the gold standard so that money became baseless. Inflation naturally was a result of going baseless. We see this year after year with the housing market. Obviously, this baseless currency serves some purpose. It would be nearly impossible to carry around a house full of heavy metals. With PAIR, the limits of real-world usability are dissipated.

The PAIR protocol  or PAIRx  allows users to mint  PAIR. PAIRx  runs on the fastest and most secure internet ever built specifically for the blockchain. PAIRx shows up as an ephemeral pop up app for the user.  The ephemerality of the app ensures access to privacy.


## First Principles
## Concepts

Mobile applications, the web and the Internet are at a cross roads. Censorship is now running rampant on social media while the decentralization
technology now allows us to own our own data, profiles, and associations, etc. The projects in the repository aim to provide a foundation for new
private and secure composable DApps (decentralized applications) which may include identity for social media networks, as well as any other
application requiring on-chain/off-chain interaction.  These projects leverage new progressive web app technology which bypasses app stores as well
being installed to the home screen for short or long term use. In addition, integrated wallets and automated payment can be facilated via off-chain
execution environment within the infrastructure.  With the advent of DeXs (decenbralized exchanges) leveraging
[Auotmated Market Makers](https://medium.com/dragonfly-research/what-explains-the-rise-of-amms-7d008af1c399), we'll see an explosion of new composed
applications which can be community funded.

Walled gardens and closed ecosystems will be pushed aside as platforms such as this one emerge.  We're leveraging open access is emerging through the use of
[progressive web apps](https://web.dev/progressive-web-apps/). We expect these kind of apps to completely eliminate the need for App Stores.

We seek to enable automated purchase of service via these "ephemeral" web apps so that consumers can once again become sovereign users of the Internet.

Our initiative has multiple components and core projects:
1. [An Ephemeral App Generator](https://gitlab.com/Crypteriat/ephemeralapps/create_ephemeral_app). This project generates a "base" DApp leveraging
ReactJS (AlephJS) for the user interface.
2. [A Link Generator to allow Websites and Ephemeral Web Apps to Create publishable and consumable links](https://gitlab.com/Crypteriat/ephemeralapps/epsilon).
The link generation is leveraged for composition of DApps. Unlike, composable smart contracts, ephemeral links enable DApps themselves to be
composable.
3. [An Automated Wallet](https://gitlab.com/Crypteriat/ephemeralapps/40-acres-a-mule-wallet). The wallet automates payment for on-chain and off-chain
services.
3. [An Ancestor Pilot DApp using a Cross-Chain Smart Contract which enables Account managment by publishing an Ephemeral Identity Link](rhttps://gitlab.com/Crypteriat/ephemeralapps/identitymanager). The pilot DApp is the basis for the generating the base DApp.

We're working hard toward creating an infrastructure for the creation and operation of composable ephemeral web apps. Crypteriat will enable a modern "fog" or "mist" services model.

![Vision](./docs/Vision.png)

The core notion of an ephemeral web app is one that can be composed by independent apps. For example, a "profile" application can be offered from a
base app to "upper" micro-blogging (e.g. "tweet"), chat, posting, or portfolio sharing social applications. There many thousands of possibilities for
ephemeral apps.  We use the terminology `ancestor` and `descendant` to describe the relationship between these linked applications
([see:http://www-math.ucdenver.edu/~wcherowi/courses/m4408/glossary.html#Digraph](http://www-math.ucdenver.edu/~wcherowi/courses/m4408/glossary.html#Digraph)).

### Some examples:

![Vision](./docs/Examples.png)

This framework can support endless types of applications -- especially since DApps can be considered a super-set of Web Apps. In addition to
traditional social media like messaging, micro-blogging, photo sharing, etc. this framework could be used for automated wallets, sharing portfolios
within a social network, [virtual voting](https://gitlab.com/Crypteriat/ephemeralapps/virtual_voting/), trading bots, and DeFi DeXs/Rebalancers.
Sovereignty identity can be maintained so that individuals retain their privacy to thwart censorship and surveillance capitalism.

## Key Documents

### [Whitepaper](./whitepaper.md)
### [Yellowpaper](./yellowpaper.md)
### [FAQ](./faq.md)

