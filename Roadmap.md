# Roadmap

<p style="display: flex; text-align:center; margin: auto;" >
<a href="https://gitcoin.co/grants/3384/secure-remote-password-with-ethereum-name-service"><img width="500" height="700"  align="middle" alt="Composeable" src="./docs/Sovereign_Security.svg"><a/>
<p/>

### Compose Virtual Machine (CVM)

- Create standard Dev environment with Rust using NixOS to run on any Intel-backed computer for developers. Required so anyone completing work related to this device does NOT require an [iMynCoin terminal](./Glossary.md). This environment is going to be used for the development of the Compose Virtual Machine.

- Create and define Compose Virtual Machine. Frontend: React + Typescript, Middle: DenoJS, Backend: smart contract code run on a bridge between EVM and Lightning Bitcoin point time lock contracts and discrete log contracts?.

### iMynCoin Terminal

- Deliver a sovereign securtiy device that acts as an authentication agent running 24/7 using Secure Remote Password technology ([SRP](./Glossary.md)). Provides various services such as running the CVM, price feeds, issuing [Pair](./Glossary.md) tokens, and improving overall girth of lighting network.

- **Congealed Network:** Distribute iMynCoin terminals around the world and grow the congealed network. These iMynCoin boxes will be placed inside gold and silver vaults as well as user homes. Formulates concept of fog computing.

### 1155+ Smart Contracts

- Set standard methods for the Defi space to adopt and allow a bridge for crypto users to utilize throughout all of the DeFi protocols. Currently, we calling this "1155+" because it uses the base methods defined in [EIP-1155](https://eips.ethereum.org/EIPS/eip-1155) along with the following functions:
    - Ownership/Role based model for instatiating smart contract
    - **Wrapping** - Allow assets like Bitcoin and others from different blockchains to be wrapped into a token, like WBTC, which then be deployed into various DeFi platforms
    - **Composition** - Enable the composition of multiple assets. For example, when Pair is staked in PairX, it then returns a new LP token, this token represents the staked amount in PairX. This new LP token can then be deployed into another DeFi platform and generate yield. This is the concept of composition DeFi. Within this composition method, sub-functions can include assign, eject, and quote-status, representing the current status of deployed assets.
    - **L1/L2 Rollup** - Rollup certain assets from layer1 into layer-2 solutions and rolldown assets from L2 to L1. Rollup is common and is important to implement because these layer-2 solutions allows transactions with faster block times and lower gas fees.
    - **Atomic Swaps** - Perform swaps between cross-chain assets. Initially we are implementing a bridge between LBTC and EVM so these atomic swaps will be occuring between LBTC and ERC-20 tokens. However, we plan on expanding this capability to allow atomic swaps with all the top blockchains.

From these core components, events will be emmitted from every call which enables us to monitor and seek the best protocol for generating yield on crpyto assets.

### Composeable DApps

- Develop ephemeral and progressive web apps ([PWAs](./Glossary.md)). PairX will be our primary [DApp](./Glossary.md). Our goal here is to be fully decentralized, we do NOT want to be censored by big tech. Ephemeral apps and PWAs will allow a generation of a temporary website that is "pulled-up" and "teared down" whenever a user hits the page. This will be built with the same core components of the [CVM](./Glossary.md).

<br>

- Decentralized Git
  .sponsorship.[tj]s bounty files
  Project bounty network
- Freemium Internet
  B/W vending machine
  Transit Key infrastructure
  Jitterbug measurement
  Congealed Payment System
- 1155+ Contracts
- PAIR Stablecoin
- Congealed Network
- IMynCoin
  Congealed Network
  Rust Environment
